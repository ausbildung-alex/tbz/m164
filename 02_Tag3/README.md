# Tag 3

[Toc]

| Datentyp | MariaDB (MySQL) | Beispiel | Bemerkung / Einstellungen |
|----------|------------------|----------|---------------------------|
| Ganze Zahlen | INT | -154 oder 154 | Range von -2147483648 bis 2147483647 |
| Natürliche Zahlen | UNSIGNED INT | 123 | Nur positive Zahlen |
| Festkommazahlen (Dezimalzahlen) | DECIMAL | 12.3 | exakte Zahlen |
| Aufzählungstypen | ENUM | 'Rot', 'Blau' | Vordefinierte Werte |
| Boolean (logische Werte) | BOOLEAN | TRUE, FALSE | 0 oder 1 |
| Zeichen (einzelnes Zeichen) | CHAR | 'A' | Feste Länge |
| Gleitkommazahlen | FLOAT | 123.45 | Es gibt auch Double |
| Zeichenkette fester Länge | CHAR | 'Test' | Range von 0 bis 255 |
| Zeichenkette variabler Länge | VARCHAR | 'Text' | extrem grosse Länge  |
| Datum und/oder Zeit | DATETIME | 2024-05-28 14:42:56 | Man kann auch CURRENT_TIMESTAMP verwenden|
| Zeitstempel | TIMESTAMP | 2024-05-28 14:42:56 | Automatischer Zeitstempel |
| Binäre Datenobjekte variabler Länge (z.B. Bild) | BLOB | Binärdaten | Bis 4GB |
| Verbund | SET | 'BMW,Audi'  | Kann maximal 64 Sachen verbinden |
| JSON | JSON | {"key": "value"} | JSON-Daten |
