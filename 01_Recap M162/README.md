# Recap M162

[TOC]

## Fragen
Hier werden die Repetitionsfragen gelöst

### 1. Welche Stufen gibt es bei der Wissenstreppe? Nennen Sie diese der Reihe nach und machen Sie ein Beispiel mit einem Wechselkurs.
- **Zeichensatz** = Buchstaben oder Zahlen
- **Daten** = Nachrichten, heisst Zeichen in einer Reihenfolge
- **Information** = Daten, welche eine Information haben
- **Wissen** = Verknüpfungen zwischen Daten
- **Kompetenz** = Ausführen
### 2. Wie werden Netzwerk-Beziehungen im logischen Modell abgebildet?
Die Knoten repräsentieren Tabellen und Kanten repräsentieren die Beziehungen. 
### 3. Was sind Anomalien in einer Datenbasis? Welche Arten gibt es?
Eine Anomalie ist ein Problem inerhalb einer Datenbank, welches durch das Löschen, Einfügen oder Aktualisieren der Daten entstehen/auftreten kann. 
### 4. Gibt es redundante "Daten"? Warum?
Redudante Daten, sind Daten, welche Informationen doppelt speichern. Auch genannt Datenredudanz.
### 5. Datenstrukturierung: Welche zwei Aspekte können strukturiert werden?  Welche Kategorien (Abstufungen) gibt es bei der Strukturierung?  Und wie müssen die Daten in einer DB strukturiert sein?

Es gibt : 
- **Unstrukturierte Daten** = Texte, Bilder
- **Semistrukturierte Daten** = Excel, JSON oder XML Dateien 
- **Strukturierte Daten** = Eine Datenbank
### 6. Beschreiben das Bild mit den richtigen Fachbegriffen

